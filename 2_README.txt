=====================================================================
CS380 GPU and GPGPU Programming
KAUST, Spring Semester 2013
Programming Assignment #2
Phong Shading and Procedural Texturing
=====================================================================

The framework itself can already do the following main things:

1. You can switch between different geometric objects.

2. Phong lighting of vertices in the vertex shader,
   which is then interpolated over the interior of triangles.
   This results in Phong lighting with Gouraud_shading, but NOT
   Phong shading!

See:
http://en.wikipedia.org/wiki/Gouraud_shading
http://en.wikipedia.org/wiki/Phong_shading

3. A sphere of varying tessellation resolution (number of triangles) is built
   by starting with an icosahedron and then subdividing each triangle into four
   triangles, then again subdividing further, and so on...

The main tasks of the assignment are:

1. Modify the lighting (point 2. above) from Phong lighting+Gouraud shading to Phong lighting+Phong shading
Move lighting computations from the vertex shader to the fragment shader
to do Phong shading instead of Gouraud shading (lighting per-fragment instead of per-vertex).
For this, the Phong lighting model must be evaluated in the fragment shader, not
in the vertex shader like it is already implemented in the framework.

2. Perform different kinds of procedural shading (in the fragment shader!):
Implement the procedural shaders described in the following chapters of the GLSL book:
- 11.1 Stripes
- 11.3 Lattice
Use the texture variable texPosGS to implement the examples of chapters
11.1 and 11.3 in the "OpenGL Shading Language" book.
Provide key mappings to allow the user to switch between these examples.

optional: implement (procedural) bump mapping (normal mapping) as in chapter 11.4

3. Submit your program and a report including the comparison of Phong and Gouraud shading and results of the different procedural shading methods.


NOTE: Put your shaders in the same directory as the executable file OR modify the shader's filename in the loader to include the directory where it is.