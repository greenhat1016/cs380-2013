=====================================================================
CS380 GPU and GPGPU Programming
KAUST, Spring Semester 2013
Programming Assignment #4 
Conjugate Gradient Linear Systems Solver
=====================================================================

Task: 

1.Implement the conjugate gradient (CG) method in CUDA:
  http://en.wikipedia.org/wiki/Conjugate_gradient_method

  Provided are a hard-coded small 4x4 matrix, a .txt file
  containing a large 1746x1746 sparse matrix, a loader
  readMatrix() function and a skeleton CG procedure.
  Note that here for the actual computation, this matrix is treated
  as dense matrix, not as sparse matrix!

  The loader takes the majority as parameter:
  if you want row majority call it with ROW_MAJORITY;
  if you need column majority call it with COLUMN_MAJORITY.

  If you need padding (for performance optimization) you have
  to modify the loader or re-sort the matrix.

  To check your results, the correct x-vector is provided for all
  datasets and loaded into the 'gold' array. Due to numerical
  differences your result might slightly vary, so please set the
  EPSILON value accordingly.

  To reach maximum points you have to use shared memory for the vector
  in the matrix-vector-multiplication and perform vector reduction using
  shared memory as well, as discussed in the lecture!

  You can get bonus points for additional performance optimizations,
  i.e., taking advantage of the sparseness of the matrix and/or padding.

2.Submit your program and a report including performance measurements 
  comparing the algorithm and the performance with/without shared memory.

Further Notes:
=============
The framework was tested with CUDA 5.0 only but should work
with older versions. If you experience any problems
please let us know.


