#version 120

uniform vec3 LightPosition;

const float SpecularContribution = 0.3;
const float DiffuseContribution  = 1.0 - SpecularContribution;

varying float lightIntensity;
varying vec3 texPos;

void main(void)
{
	// TODO 1:
	//
	// move lighting computations from here (i.e., vertex shader)
	// to fragment shader to do Phong shading (interpolation) with
	// the Phong lighting model instead of Gouraud shading with the
	// Phong lighting model. For more information see
	//
	// http://en.wikipedia.org/wiki/Gouraud_shading
	// http://en.wikipedia.org/wiki/Phong_shading

    vec3 ecPosition = vec3(gl_ModelViewMatrix * gl_Vertex);
    vec3 tnorm      = normalize(gl_NormalMatrix * gl_Normal);
    vec3 lightVec   = normalize(LightPosition - ecPosition);
    vec3 reflectVec = reflect(-lightVec, tnorm);
    vec3 viewVec    = normalize(-ecPosition);
    float diffuse   = max(dot(lightVec, tnorm), 0.0);
    float spec      = 0.0;

    if (diffuse > 0.0)
    {
        spec = max(dot(reflectVec, viewVec), 0.0);
        spec = pow(spec, 16.0);
    }

    lightIntensity  = DiffuseContribution * diffuse +
                      SpecularContribution * spec;

    texPos = gl_Vertex.xyz;

    gl_Position     = ftransform();
}

