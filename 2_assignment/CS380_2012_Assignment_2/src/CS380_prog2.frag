#version 120

varying vec3 texPosGS;
varying float lightIntensityGS;

void main(void)
{
    // TODO 2:
    //
    // Use texPosGS to implement the examples of chapters
    // 11.1 and 11.3 in the "OpenGL Shading Language"
    // book. Provide key mappings in "CS380_prog2.c" to switch
    // between these examples.
    //
    // optional: implement (procedural) bump mapping (normal mapping)
    //           as in chapter 11.4

    float val = lightIntensityGS;
    gl_FragColor = vec4 (val, val, val, 1.0);
}

