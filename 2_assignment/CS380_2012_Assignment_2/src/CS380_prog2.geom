#version 120
#extension GL_EXT_geometry_shader4 : enable
 
varying in float lightIntensity[3];
varying in vec3 texPos[3];

varying out float lightIntensityGS;
varying out vec3 texPosGS;

void main(void)
{
   for(int i=0; i< gl_VerticesIn; ++i){
        lightIntensityGS = lightIntensity[i];
	    texPosGS = texPos[i];
	    gl_Position = gl_PositionIn[i];
	    EmitVertex();
   }
   
   EndPrimitive();	
}

