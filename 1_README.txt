=====================================================================
CS380 GPU and GPGPU Programming
KAUST, Spring Semester 2013
Programming Assignment #1
Querying the Graphics Card (OpenGL and CUDA)
=====================================================================

Tasks:

1. Download CUDA 5.0 from 
https://developer.nvidia.com/cuda-downloads
install it and restart your PC. Open the assignment file in Visual Studio 2010. Try to compile and run it. 


2. OPENGL

Query and print (to console)
- OpenGL version and available extensions:
- GL vendor, renderer, and version using glGetString()

Find out how to query extensions with GLEW (http:glew.sourceforge.net/, http:www.opengl.org/registry/).
Query and print whether your GPU supports:

 - 3D textures
 - texture arrays
 - support for frame buffer objects
 - support for the GLSL Shading Language, and which version of GLSL is supported

3. OPENGL Limits

Query and print GPU OpenGL limits (using glGet(), glGetInteger() etc.):
 - maximum number of vertex shader attributes
 - maximum number of varying floats
 - number of texture image units (in vertex shader and in fragment shader, respectively)
 - maximum 2D texture size
 - maximum 3D texture size
 - maximum number of draw buffers

4. CUDA

Query and print CUDA functionality:
 - number of CUDA-capable GPUs in your system using cudaGetDeviceCount()
 - CUDA device properties for every GPU found using cudaGetDeviceProperties():
 - device name
 - compute capability: driver version, runtime version, major and minor rev. numbers
 - multi-processor count
 - clock rate
 - total global memory
 - shared memory per block
 - num registers per block
 - warp size (in threads)
 - max threads per block

5. Submit your program and a short report including the text output your program produces on your machine