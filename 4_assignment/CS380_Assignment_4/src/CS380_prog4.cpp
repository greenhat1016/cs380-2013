#include "CS380_prog4.h"

// utilities and system includes
#include <cuda_runtime.h>
#include <helper_cuda.h>

#include <iostream>
#include <fstream>
#include <cassert>

enum MAJORITY {
	ROW_MAJOR = 0,
	COLUMN_MAJOR = 1
};

#define EPSILON 0.0001f

// This is the CUDA functions to handle allocation and launching the kernels
extern "C" void computeConjugateGradient( float *d_A, float *d_b, float *d_x, int dim );


int readMatrix( char* filename, float* &matrix, unsigned int *dim = NULL, int majority = ROW_MAJOR )
{
	unsigned int w, h, x, y, num_entries;
	
	float val;
	
	std::ifstream file( filename );
	
	if ( file )
	{
		file >> h >> w >> num_entries;
		std::cout << w << " " << h << " " << num_entries << "\n";
		
		assert( w == h || w == 1 || h == 1 );
		
		if( dim != NULL ) *dim = std::max( w, h );
		
		matrix = new float[ w * h ];
		memset( matrix, 0, w * h * sizeof( float ) );
		
		unsigned int i;
		for( i = 0; i < num_entries; i++ ){
		
			if( file.eof() ) break;
			
			file >> y >> x >> val;
			
			if( majority == ROW_MAJOR ){
				
				matrix[ w * y + x ] = val;
			
			} else if( majority == COLUMN_MAJOR ){
				
				matrix[ h * x + y ] = val;
			}
		}
		file.close();

		if( i == num_entries )
			std::cout << "\nFile read successfully\n"; 
		else
			std::cout << "\nFile read successfully but seems defective:\n num entries read = " << i << ", entries epected = " << num_entries << "\n"; 
		
		// print first few elements
		if( w == h ){
			for( unsigned int i = 0; i < w; i++ ){
				
				printf("\n");
				for( unsigned int j = 0; j < h; j++ ){
					
					printf("%.2f ", matrix[ j + w * i ] );
				}
			}	
		}
		else{	
				
			printf("\n");
			for( unsigned int j = 0; j < h; j++ ){
				
				printf("%.2f ", matrix[ j ] );
			}
		}

	} else {
		
		std::cout << "Unable to open file\n";
		return false;
	}
	
	return true;
}


/******************************************************************************/
/*
 * Main
 *
 ******************************************************************************/

int main( int argc, char **argv )
{    
	unsigned int dim;
	float *h_A = NULL, *h_b = NULL, *h_x = NULL, *gold = NULL;
	bool success = true;
	
	// read matrices
	int matrixSet = 0;		// TODO: Set this variable to choose which matrix and vectors to use as inputs.
	switch( matrixSet )
	{
		case( 1746 ):
			success = readMatrix( (char *)"matrices/A.txt", h_A, &dim,COLUMN_MAJOR );	
			success = success && readMatrix( (char *)"matrices/b.txt", h_b);	
			success = success && readMatrix( (char *)"matrices/x.txt", gold );
			break;
		default:
			h_A = new float[ 16 ]; h_b = new float[ 4 ]; gold = new float [ 4 ]; dim = 4;
			h_A[ 0 ] = 6.25f; h_A[ 4 ] = 3.5f; h_A[ 8  ] = 4.0f; h_A[ 12 ] = 5.5f;
			h_A[ 1 ] = 3.5f; h_A[ 5 ] = 5.25f; h_A[ 9  ] = 0.5f; h_A[ 13 ] = 4.5f;
			h_A[ 2 ] = 4.0f; h_A[ 6 ] = 0.5f; h_A[ 10 ] = 10.0f; h_A[ 14 ] = 2.0f;
			h_A[ 3 ] = 5.5f; h_A[ 7 ] = 4.5f; h_A[ 11 ] = 2.0f; h_A[ 15 ] = 7.25f;

			h_b[ 0 ] = 7.0f; h_b[ 1 ] = 5.5f; h_b[ 2 ] = 11.0f; h_b[ 3 ] = 6.75f;

			gold[ 0 ] = -0.54545f; gold[ 1 ] = 0.90909f; gold[ 2 ] = 1.18182f; gold[ 3 ] = 0.45455f;
			break;
	}
	
	if( !success ){
	
		std::cout << "File input error";
		return 0;
	}
 
	// init CUDA
	checkCudaErrors( cudaSetDevice( gpuGetMaxGflopsDeviceId() ) );
	
	// compute
	h_x = new float[ dim ];

	computeConjugateGradient( &h_A[0], &h_b[0], &h_x[0], dim );
	
	// compare result to gold
	for( unsigned int i = 0; i < dim; i++ ){
		
		float abs_diff = h_x[ i ] - gold[ i ];
		abs_diff = abs_diff > 0.0f ? abs_diff : -abs_diff;
		
		std::cout << "\nx_" << i << ": " << h_x[ i ] << " " << gold[ i ] << " " << abs_diff;
		
		if( abs_diff > EPSILON ){
			
			success = false;
		}
	}
	
	if( success ) {
		
		std::cout << "\nComputation successful\n";
		
	} else {
		
		std::cout << "\nComputation failed\n";
	}
	 
	if (h_A)
		delete h_A;
	if (h_b)
		delete h_b;
	if (h_x)
		delete h_x;
	if (gold)
		delete gold;

	std::cout << "\nPress return key to close";
	std::cin.get();

	return 0;
}
