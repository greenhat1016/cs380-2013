#ifndef _CS380_PROG4_KERNEL_CU_
#define _CS380_PROG4_KERNEL_CU_

#include <cuda_runtime_api.h>
#include "helper_cuda.h"
#include "helper_math.h"

#define EPSILON 0.01f

enum VEC_OP{

	NONE = -1,
	CL_ADD = 0,
	CL_SUB = 1,
	CL_MULT = 2,
	CL_MAX = 3,
	CL_MIN = 4,
	CL_ABS = 5
};


// CUDA helper ----------------------------------------------------------------
//
extern "C"
int iDivUp( int a, int b ){
    return (a % b != 0) ? (a / b + 1) : (a / b);
}


// clVecOp --------------------------------------------------------------------
//
__global__ void
_cl_vector_op_( int op, float fac0, float fac1, float *a, float *b, float *x, int dim )
{
	/* TASK 1.1: implement the vector operations
	
	 	x = fac0 * a (op) fac1 * b
	
		with op = {+,-,*}.
	*/
}


// clMatVec -------------------------------------------------------------------
//
__global__ void
_cl_matrix_vector_( int op, float *A, float *b, float *c, float *x, int dim )
{
	/* TASK 1.2: implement the matrix vector multiplication
	
		x = A * b (op) c
	
		with op = {+,-,*}.
	*/
}


// clVecReduce ----------------------------------------------------------------
//
float _cl_vector_reduce_( int op, float* d_a, float *d_b, float* d_x, int dim, int nBlocks, int nThreads )
{
	/* TASK 1.3: implement reduction combined with simultaneous evaluation of the specified operator
	
		x = a (op) b
	
		with op = {+,-,*}.

	   TASK 1.4 (optimization): use shared memory
	*/

	return 1.0f;
}


// ----------------------------------------------------------------------------
//
extern "C" 
void computeConjugateGradient( float *h_A, float *h_b, float *h_x, int dim )
{
	int nThreads = 128;
	int nBlocks = iDivUp( dim, nThreads );

	float *d_A, *d_b, *d_x, *d_r, *d_p, *d_q, *d_tmp;
	float alpha, beta, rho;

	checkCudaErrors( cudaMalloc( (void**) &d_A, dim * dim * sizeof( float ) ) );
	checkCudaErrors( cudaMalloc( (void**) &d_b, dim * sizeof( float ) ) );
	checkCudaErrors( cudaMalloc( (void**) &d_x, dim * sizeof( float ) ) );
	checkCudaErrors( cudaMalloc( (void**) &d_r, dim * sizeof( float ) ) );
	checkCudaErrors( cudaMalloc( (void**) &d_p, dim * sizeof( float ) ) );
	checkCudaErrors( cudaMalloc( (void**) &d_q, dim * sizeof( float ) ) );
	checkCudaErrors( cudaMalloc( (void**) &d_tmp, dim * sizeof( float ) ) );
	
	// initialize x to 0-vector
	for (int i = 0; i < dim; ++i)
		h_x[i] = 0.0f;

	checkCudaErrors( cudaMemcpy( d_A, h_A, dim * dim * sizeof( float ), cudaMemcpyHostToDevice ) );
	checkCudaErrors( cudaMemcpy( d_b, h_b, dim * sizeof( float ), cudaMemcpyHostToDevice ) );
	checkCudaErrors( cudaMemcpy( d_x, h_x, dim * sizeof( float ), cudaMemcpyHostToDevice ) );

	/* init the Conjugate Gradient Method (CG) */
	_cl_matrix_vector_<<< nBlocks, nThreads, dim * sizeof( float ) >>>( CL_SUB, d_A, d_x, d_b, d_r, dim );
	checkCudaErrors( cudaDeviceSynchronize() );
	
	_cl_vector_op_<<< nBlocks, nThreads >>>( NONE, -1.0f, 0.0f, d_r, NULL, d_r, dim );
	checkCudaErrors( cudaDeviceSynchronize() );
	
	_cl_vector_op_<<< nBlocks, nThreads >>>( NONE,  1.0f, 0.0f, d_r, NULL, d_p, dim );
	checkCudaErrors( cudaDeviceSynchronize() );

	// CG needs max dim iterations
	for( int i = 0; i < dim; i++ ){
	
		/* Conjugate Gradient Method (Wikipedia) */

		rho = _cl_vector_reduce_( CL_ADD, d_r, d_r, d_tmp, dim, nBlocks, nThreads );
		checkCudaErrors( cudaDeviceSynchronize() );

		if( rho < EPSILON ) break;
		
		_cl_matrix_vector_<<< nBlocks, nThreads, dim * sizeof( float ) >>>( NONE, d_A, d_p, NULL, d_q, dim );
		checkCudaErrors( cudaDeviceSynchronize() );
		
		alpha = rho / _cl_vector_reduce_( CL_ADD, d_p, d_q, d_tmp, dim, nBlocks, nThreads );
		checkCudaErrors( cudaDeviceSynchronize() );
		
		_cl_vector_op_<<< nBlocks, nThreads >>>( CL_ADD, 1.0f, alpha, d_x, d_p, d_x, dim );
		checkCudaErrors( cudaDeviceSynchronize() );
		
		_cl_vector_op_<<< nBlocks, nThreads >>>( CL_ADD, 1.0f, -alpha, d_r, d_q, d_r, dim );
		checkCudaErrors( cudaDeviceSynchronize() );

		beta = _cl_vector_reduce_( CL_ADD, d_r, d_r, d_tmp, dim, nBlocks, nThreads ) / rho;
		checkCudaErrors( cudaDeviceSynchronize() );
		
		_cl_vector_op_<<< nBlocks, nThreads >>>( CL_ADD, 1.0f, beta, d_r, d_p, d_p, dim );
		checkCudaErrors( cudaDeviceSynchronize() );
	}

	// copy device to host
	checkCudaErrors( cudaMemcpy( h_x, d_x, dim * sizeof( float ), cudaMemcpyDeviceToHost ) );

	// release device memory
	checkCudaErrors( cudaFree( d_A ) );
	checkCudaErrors( cudaFree( d_b ) );
	checkCudaErrors( cudaFree( d_x ) );
	checkCudaErrors( cudaFree( d_r ) );
	checkCudaErrors( cudaFree( d_p ) );
	checkCudaErrors( cudaFree( d_q ) );
	checkCudaErrors( cudaFree( d_tmp ) );
}


#endif // #ifndef _CS380_PROG4_KERNEL_CU_
