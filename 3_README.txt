=====================================================================
CS380 GPU and GPGPU Programming
KAUST, Spring Semester 2013
Programming Assignment #3
Image Processing with GLSL and CUDA
=====================================================================

Tasks:

1. texture mapping
- read a sample texture from the folder "textures/", use
  any library or sample code you like (e.g., libjpeg). you
  can also convert the texture to a simpler format before
  you read it. you can also look at the CUDA SDK examples and
  use image loading code from there.
- assign texture coordinates:
  assign texture coordinates manually for the box model (on the cpu)
- render the box with the texture assigned to it

2. image processing with GLSL

- implement simple operations to adjust brightness, contrast, and saturation of the texture, as described in chapter 19.5 in the GLSL book
- implement smoothing, edge detection, and sharpening as described in chapter 19.7 in the GLSL book.

IMPLEMENTATION HINTS:
use TWO rendering passes
pass 1: render the processed image into a target texture attached
        to an OpenGL frame buffer object (FBO). this target texture
        is not the source texture!
        perform the actual image processing operation in this rendering pass.

pass 2: use the resulting texture for texture mapping.

3. image processing with CUDA

- implement the same image processing operations as in point 2 (brightness, contrast, saturation, smoothing, edge detection, sharpening), but this time using CUDA

Use one of the two variants:
a) Simple c-style array as input for CUDA:
   use a simple c-style array as input for your CUDA kernels
   use cudaMalloc to allocate and cudaMemcpy to initialize
   device memory. Do not confuse this with the cudaArray type.

b) Texture as input for CUDA: 
   use a texture as input for your CUDA kernels.
   For this, you need the above mentioned cudaArray type.

4. Submit your program and a report including results for the different image processing operations comparing the GLSL and the CUDA version.
   

All settings must be accessible from your program without the need to modify the source code. 
Provide key mappings to switch between the image processing filters and operations and GLSL vs. CUDA

The provided textures are from http://www.grsites.com/
You may add other textures as well