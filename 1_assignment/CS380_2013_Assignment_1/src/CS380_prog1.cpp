//
// CS 380 - GPU and GPGPU Programming
// KAUST, Spring Semester 2013
//
// Programming Assignment #1
//
// built from "Simple OpenGL" example in NVIDIA CUDA SDK
//

// includes

// system
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <math.h>

// includes for GL
#include <glew.h>
#include <glut.h>

// includes for CUDA
#include <cuda_runtime.h>

// constants
#define horizontalLine "   -------------------------------------------------------------------------\n"

const unsigned int window_width = 512;
const unsigned int window_height = 512;

// mouse tracking variables
int mouse_old_x, mouse_old_y;
int mouse_buttons = 0;
float rotate_x = 0.0, rotate_y = 0.0;
float translate_z = -3.0;

// display callback for rendering
//
void display()
{
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	// set view matrix
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();

	// put rendering code here
	//...

	glutSwapBuffers();
	glutPostRedisplay();
}

// key press callback
//
void keyboard(unsigned char key, int x, int y)
{
	switch(key) {

		// put further key handling here
		//...

		// escape key quits application
		case(27):
			exit(0);
			break;
	}
}

// mouse button callback
//
void mouse(int button, int state, int x, int y)
{
	if (state == GLUT_DOWN) {
		mouse_buttons |= 1<<button;
	} else if (state == GLUT_UP) {
		mouse_buttons = 0;
	}

	mouse_old_x = x;
	mouse_old_y = y;

	// can put further mouse button handling here
	//...

	glutPostRedisplay();
}

// mouse motion callback
//
void motion(int x, int y)
{
	float dx, dy;
	dx = x - mouse_old_x;
	dy = y - mouse_old_y;

	if (mouse_buttons & 1) {
		rotate_x += dy * 0.2;
		rotate_y += dx * 0.2;
	} else if (mouse_buttons & 4) {
		translate_z += dy * 0.01;
	}

	mouse_old_x = x;
	mouse_old_y = y;

	// can put further mouse motion handling here
	//...

	glutPostRedisplay();
}

// query GPU functionality we need for OpenGL, return false when not available
//
bool QueryGPUCapabilitiesOpenGL()
{
	// for all the following:
	// Google and read up on extensions and concepts needed here that you do not know!

	// =============================================================================
	//TODO:
	// query and print (to console) OpenGL version and extensions:
	// - query and print GL vendor, renderer, and  version using glGetString()
	//
	// find out how to query extensions with GLEW:
	//   http://glew.sourceforge.net/, http://www.opengl.org/registry/
	// - query and print whether your GPU supports:
	//   - 3D textures
	//   - texture arrays
	//   - support for frame buffer objects
	//   - support for the GLSL Shading Language, and which version of GLSL is supported
	//   - GLSL geometry shaders. see http://www.opengl.org/registry/specs/ARB/geometry_shader4.txt
	//     if you cannot use this extension look for GL_EXT_geometry_shader4 instead and update your graphics driver!

	//TODO:
	// query and print GPU OpenGL limits (using glGet(), glGetInteger() etc.):
	// - maximum number of vertex shader attributes
	// - maximum number of varying floats
	// - number of texture image units (in vertex shader and in fragment shader, respectively)
	// - maximum 2D texture size
	// - maximum 3D texture size
	// - maximum number of draw buffers
	// =============================================================================

	return true;
}

// query GPU functionality we need for CUDA, return false when not available
//
bool QueryGPUCapabilitiesCUDA()
{
	// for all the following:
	// Google and read up on concepts needed here that you do not know!



	// Device Count
	int devCount;
	// Get the Device Count
	cudaGetDeviceCount(&devCount);

	// Print Device Count
	printf("Device(s): %i", devCount);

	// =============================================================================
	//TODO:
	// query and print CUDA functionality:
	// - number of CUDA-capable GPUs in your system using cudaGetDeviceCount()
	// - CUDA device properties for every found GPU using cudaGetDeviceProperties():
	//   - device name
	//   - compute capability
	//   - multi-processor count
	//   - clock rate
	//   - total global memory
	//   - shared memory per block
	//   - num registers per block
	//   - warp size (in threads)
	//   - max threads per block
	// =============================================================================

	return true;
}

void InitGLUT(int argc, char **argv)
{
	glutInit(&argc, argv);

	glutInitDisplayMode(GLUT_RGBA | GLUT_DOUBLE);

	glutInitWindowSize(window_width, window_height);
	glutCreateWindow("GPU and GPGPU Programming");

	glutDisplayFunc(display);
	glutKeyboardFunc(keyboard);
	glutMotionFunc(motion);
}

bool InitGL(int argc, char **argv)
{
	InitGLUT(argc, argv);

	// check functionality that we need
	if ( !QueryGPUCapabilitiesOpenGL() ) {
		return false;
	}

	// default initialization
	glClearColor(0.0, 0.0, 0.0, 1.0);
	glDisable(GL_DEPTH_TEST);

	// configure OpenGL viewport
	glViewport(0, 0, window_width, window_height);

	// configure OpenGL projection matrix
	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();
	gluPerspective(60.0, (GLfloat)window_width / (GLfloat) window_height, 0.1, 10.0);

	return true;
}

bool InitCUDA(int argc, char **argv)
{
	// check functionality that we need
	if ( !QueryGPUCapabilitiesCUDA() ) {
		return false;
	}

	return true;
}

bool Run(int argc, char** argv)
{
	// quit when not everything supported that we need
	if (!InitGL(argc, argv)) {
		return false;
	}

	// quit when not everything supported that we need
	if (!InitCUDA(argc, argv)) {
		return false;
	}

	// register callbacks
	glutDisplayFunc(display);
	glutKeyboardFunc(keyboard);
	glutMouseFunc(mouse);
	glutMotionFunc(motion);

	// start rendering mainloop
	glutMainLoop();

	return true;
}

int main(int argc, char** argv)
{
	Run(argc, argv);
}

