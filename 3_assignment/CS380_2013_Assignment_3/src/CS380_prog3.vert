#version 120

uniform int EnableCylinderMapping;

varying vec3 ecPosition;
varying vec3 normal;
varying vec2 texCoord0;

void main(void)
{
    ecPosition  = vec3(gl_ModelViewMatrix * gl_Vertex);
    normal      = gl_NormalMatrix * gl_Normal;
    gl_TexCoord[0] = gl_MultiTexCoord0;
    texCoord0 = gl_TexCoord[0].st;
    
    if(EnableCylinderMapping == 1) {    
        texCoord0.s = gl_Vertex.y/1.0;
        texCoord0.t = atan(gl_Vertex.x/gl_Vertex.z);
    }
    gl_Position = ftransform();
}

