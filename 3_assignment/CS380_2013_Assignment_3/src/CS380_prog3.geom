#version 120
#extension GL_EXT_geometry_shader4 : enable
 
varying in vec3 ecPosition[3];
varying in vec3 normal[3];
varying in vec2 texCoord0[3];

varying out vec3 ecPositionGS;
varying out vec3 normalGS;
varying out vec2 texCoord0GS;

void main(void)
{
   for(int i=0; i<gl_VerticesIn; ++i){
	    ecPositionGS = ecPosition[i];
	    normalGS = normal[i];
	    texCoord0GS = texCoord0[i];
	    gl_Position = gl_PositionIn[i];
	    EmitVertex();
   }
   
   EndPrimitive();	
}

