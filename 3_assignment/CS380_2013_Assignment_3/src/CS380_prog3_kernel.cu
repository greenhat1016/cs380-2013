#ifndef _CS380_PROG4_KERNEL_CU_
#define _CS380_PROG4_KERNEL_CU_

#ifdef _WIN32
#define WIN32
#endif 

#include <helper_math.h>
#include <helper_functions.h>


// This will output the proper CUDA error strings in the event that a CUDA host call returns an error
#define checkCudaErrors(err)           __checkCudaErrors (err, __FILE__, __LINE__)

inline void __checkCudaErrors(cudaError err, const char *file, const int line)
{
    if (cudaSuccess != err)
    {
        fprintf(stderr, "%s(%i) : CUDA Runtime API error %d: %s.\n",
                file, line, (int)err, cudaGetErrorString(err));
        exit(EXIT_FAILURE);
    }
}

// CUDA helper ----------------------------------------------------------------
//
extern "C"
int iDivUp( int a, int b )
{
    return (a % b != 0) ? (a / b + 1) : (a / b);
}

__device__ uint rgbaFloatToInt(float4 rgba)
{
    rgba.x = __saturatef(rgba.x);   // clamp to [0.0, 1.0]
    rgba.y = __saturatef(rgba.y);
    rgba.z = __saturatef(rgba.z);
    rgba.w = __saturatef(rgba.w);
    return (uint(rgba.w * 255.0f) << 24) | (uint(rgba.z * 255.0f) << 16) | (uint(rgba.y * 255.0f) << 8) | uint(rgba.x * 255.0f);
}

__device__ float4 rgbaIntToFloat(uint c)
{
    float4 rgba;
    rgba.x = (c & 0xff) * 0.003921568627f;       //  /255.0f;
    rgba.y = ((c>>8) & 0xff) * 0.003921568627f;  //  /255.0f;
    rgba.z = ((c>>16) & 0xff) * 0.003921568627f; //  /255.0f;
    rgba.w = ((c>>24) & 0xff) * 0.003921568627f; //  /255.0f;
    return rgba;
}

extern "C" 
void initInput(int width, int height, void *pDevImage, void *pImage)
{
	// TODO:
	/*
	 *	Initialize device memory for array version
	 *	and cuda arrays for texture version here
	 */
}

extern "C"
void freeTextures()
{
	// TODO:
	/*
	 * free cuda memory here
	 */
}


extern "C" 
void filter( /* input/output data, parameters, etc */ )
{
	/* 
	 * run different kernels for array and texture version here
	 */
}


#endif // #ifndef _CS380_PROG4_KERNEL_CU_
