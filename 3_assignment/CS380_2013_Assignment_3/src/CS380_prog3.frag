#version 120

uniform vec3 LightPosition;
uniform int ShaderType;	

varying vec3 normalGS;
varying vec3 ecPositionGS;
varying vec2 texCoord0GS;

float computeLightIntensity(vec3 ecPos, vec3 normal)
{
    const float SpecularContribution = 0.3;
    const float DiffuseContribution  = 1.0 - SpecularContribution;

    vec3 lightVec   = normalize(LightPosition - ecPos);
    vec3 reflectVec = reflect(-lightVec, normal);
    vec3 viewVec    = normalize(-ecPos);
    float diffuse   = max(dot(lightVec, normal), 0.0);
    float spec = 0.0;

    if (diffuse > 0.0)
    {
        spec = max(dot(reflectVec, viewVec), 0.0);
        spec = pow(spec, 16.0);
    }

    return DiffuseContribution * diffuse + SpecularContribution * spec;
}

void main(void)
{

    if( ShaderType == 0 ) {

		// TODO: 
		// Implement no image processing, just texture mapping.
        gl_FragColor = vec4( 1.0, 0.0, 0.0, 1.0 );
    
    } else {
	
		// TODO:
		// Replace the following with image processing.
        gl_FragColor = vec4( 0.0, 1.0, 0.0, 1.0 );
	}
}

